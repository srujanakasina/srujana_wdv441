<html>
    <body>
        <table>
            <tr>
                <td>
                    Title:
                </td>
                <td>
                    <?php echo $newsArticle->articleTitle; ?>
                </td>                
            </tr>
            <tr>
                <td>
                    Author:
                </td>
                <td>
                    <?php echo $newsArticle->articleAuthor; ?>
                </td>                
            </tr>
            <tr>
                <td>
                    Article Date:
                </td>
                <td>
                    <?php echo $newsArticle->articleDate; ?>
                </td>                
            </tr>
            
            <tr>
                <td colspan="2">
                    <?php echo $newsArticle->articleContent; ?>
                </td>                
            </tr>
            
        </table>
        <a href="article_list.php">Back to List</a>
    </body>
</html>