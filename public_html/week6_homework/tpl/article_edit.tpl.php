<html>
    <body>
        
        <?php if (isset($errors) && count($errors) > 0) { ?>
            <!-- html -->
            <div>
                <ul>
                    <?php foreach ($errors as $field => $errorMessage) 
                    {?>
                        <li><?php echo $errorMessage; ?></li>
                    <?php } ?>                    
                </ul>
            </div>
        <?php } ?>

        <?php //var_dump($errors); ?>
               
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
            <span><?php if (isset($errors['articleTitle'])) { ?><?php echo $errors['articleTitle']; ?><?php } ?></span>
            title: <input type="text" name="article_title" value="<?php echo $newsArticle->articleTitle; ?>"/><br>
            <span><?php if (isset($errors['articleContent'])) { ?><?php echo $errors['articleContent']; ?><?php } ?></span>
            content: <input type="text" name="article_content" value="<?php echo $newsArticle->articleContent; ?>"/><br>
            <span><?php if (isset($errors['articleAuthor'])) { ?><?php echo $errors['articleAuthor']; ?><?php } ?></span>
            author: <input type="text" name="article_author" value="<?php echo $newsArticle->articleAuthor; ?>"/><br>
            <span><?php if (isset($errors['articleDate'])) { ?><?php echo $errors['articleDate']; ?><?php } ?></span>
            date: <input type="text" name="article_date" value="<?php echo $newsArticle->articleDate; ?>"/><br>            
            <input type="hidden" name="article_id" value="<?php echo $newsArticle->articleId; ?>"/><br>
            <input type="submit" name="submit">
            <input type="submit" name="cancel" value="cancel">            
        </form>        
    </body>
</html>