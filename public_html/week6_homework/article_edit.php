<?php
require_once('inc/news_article.class.php');

$newsArticle = new NewsArticle();

if (isset($_REQUEST['article_id']) && $_REQUEST['article_id'] > 0)
{
    $newsArticle->load($_REQUEST['article_id']);
}

if (isset($_POST['cancel'])) 
{
    header("location: article_list.php?cancel");
    exit;
}

if (isset($_POST['article_id'])) 
{
    $newsArticle->setData($_POST);
    
    $errors = $newsArticle->validate();
    
    if (count($errors) == 0) 
    {
        $newsArticle->save();
        
        header("location: article_list.php?saved");
        exit;
    }
    
}

include("tpl/article_edit.tpl.php");
?>