<html>
    <body>
        
       <?php if ($message) 
        { ?>
            <div><?php echo $message; ?></div>
        <?php } ?><br>
        
        <a href="article_edit.php">Create New Article</a>
        <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="GET">
            <select name="author_filter" id="author_filter">            
                <option value="">All</option>
                <?php while ($author_row = mysqli_fetch_assoc($authorList))
                { ?>
                    <option value="<?php echo $author_row['article_author']; ?>"><?php echo $author_row['article_author']; ?></option>
                <?php } ?>
            </select><input type="text" name="search_text" id="search_text"/>            
            <input type="submit" name="filter" value="Go!">
        </form>
        <br>
        <table border="1">
            <thead>
                <th><a href="article_list.php?sort_column=article_title">Article Title</a></th>
                <th><a href="article_list.php?sort_column=article_author">Article Author</a></th>
                <th><a href="article_list.php?sort_column=article_date">Article Date</a></th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>  

            <?php while ($row = mysqli_fetch_assoc($newsList)) 
            { ?>
                <tr>
                    <td>
                        <?php echo $row['article_title']; ?>
                    </td>
                    <td>
                        <?php echo $row['article_author']; ?>
                    </td>
                    <td>
                        <?php echo $row['article_date']; ?>
                    </td>
                    <td>
                        <a href="article_edit.php?article_id=<?php echo $row['article_id']; ?>">Edit</a>
                    </td>
                    <td>
                        <a href="article_view.php?article_id=<?php echo $row['article_id']; ?>">View</a>
                    </td>
                </tr>
            <?php } ?>

<!--                
            <?php while ($row = mysqli_fetch_assoc($newsList2)) 
            { ?>
                <tr>
                    <?php foreach ($row as $column_name => $data)
                    { ?>
                        <td>
                            <?php echo $data; ?>
                        </td>                        
                    <?php } ?>                    
                </tr>
            <?php } ?>
-->

        </table>
    </body>
</html>