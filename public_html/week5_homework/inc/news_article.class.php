<?php

class NewsArticle 
{
    var $articleId;
    var $articleTitle;
    var $articleContent;
    var $articleAuthor;
    var $articleDate;

    function setData($dataArray) 
    {
        $this->articleId = $dataArray['article_id'];
        $this->articleTitle = $dataArray['article_title'];
        $this->articleContent = $dataArray['article_content'];
        $this->articleAuthor = $dataArray['article_author'];
        $this->articleDate = $dataArray['article_date'];
    }
    
    function load($articleId)
    {
        $success = false;
        
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $loadArticleSQL = "SELECT * FROM news_article WHERE article_id = " . 
                mysqli_real_escape_string($db, $articleId);            
            //var_dump($loadArticleSQL);die;
            $rs = mysqli_query($db, $loadArticleSQL);
            
            if ($rs) 
            {                
                $articleData = mysqli_fetch_assoc($rs);
//                var_dump($articleData);die;
                $this->setData($articleData);
                $success = ($this->articleId > 0 ? true : false);
            }
            else                 
            {
                echo mysqli_error($db);
                die;                
            }
        } 
        else 
        {
            echo mysqli_error($db);
            die;
        }
        
        return $success;
    }
    
    function save()
    {
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
        
            if ($this->articleId > 0) {
                // this is an update
                $articleUpdateSQL = "UPDATE news_article SET " .
                    "article_title = '" . mysqli_real_escape_string($db, $this->articleTitle) . "', " .
                    "article_content = '" . mysqli_real_escape_string($db, $this->articleContent) . "', " .
                    "article_author = '" . mysqli_real_escape_string($db, $this->articleAuthor) . "', " .
                    "article_date = '" . mysqli_real_escape_string($db, $this->articleDate) . "' " .
                    "WHERE article_id = " . mysqli_real_escape_string($db, $this->articleId);

                $rs = mysqli_query($db, $articleUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                }
            }
            else 
            {
                // this is an insert
                $articleInsertSQL = "INSERT INTO news_article SET " . 
                    "article_title = '" . mysqli_real_escape_string($db, $this->articleTitle) . "', " .
                    "article_content = '" . mysqli_real_escape_string($db, $this->articleContent) . "', " .
                    "article_author = '" . mysqli_real_escape_string($db, $this->articleAuthor) . "', " .
                    "article_date = '" . mysqli_real_escape_string($db, $this->articleDate) . "'";

                $rs = mysqli_query($db, $articleInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                } 
                else 
                {
                    $this->articleId = mysqli_insert_id($db);
                }

            }
        }
    }
    
    function validate() 
    {
        $errorsArray = array();
        
        if (empty($this->articleTitle)) 
        {
            $errorsArray['articleTitle'] = "Article Title is required.";
        }

        if (empty($this->articleContent)) 
        {
            $errorsArray['articleContent'] = "Article Content is required.";
        }

        if (empty($this->articleAuthor)) 
        {
            $errorsArray['articleAuthor'] = "Article Author is required.";
        }
      
        return $errorsArray;
    }
    
    function getArticleList($sort_column = null, $authorFilter = null, $searchText = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT * FROM news_article";

            if (!is_null($authorFilter))
            {
                $getArticleListSQL .= " WHERE article_author = '" . mysqli_real_escape_string($db, $authorFilter) . "'";
            }

            if (!is_null($searchText))
            {
                $getArticleListSQL .= " WHERE " . 
                    "article_title LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " .
                    "article_content LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " . 
                    "article_date LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " . 
                    "article_author LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%'";
            }
            //var_dump($getArticleListSQL);die;
            if (!is_null($sort_column))
            {
                $getArticleListSQL .= " ORDER BY " . $sort_column;
            }
                        
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }
    
    function getArticleList2() 
    {
        $rs = null;
            
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT article_title, article_author, article_date FROM news_article";
            
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }

    function getAuthorList() 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getAuthorListSQL = "SELECT DISTINCT article_author FROM news_article";            
            $getAuthorListSQL .= " ORDER BY article_author";
                        
            $rs = mysqli_query($db, $getAuthorListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }
    
}
?>