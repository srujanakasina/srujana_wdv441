<?php
include_once('inc/user.class.php');

$user = new User();

$userList = $user->getUsers();

$jsonArray = array();
$jsonArray['users'] = array();

while ($row = mysqli_fetch_assoc($userList)) 
{
        $jsonArray['users'][] = $row;
}

echo json_encode($jsonArray);
?>