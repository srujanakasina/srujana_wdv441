<?php  session_start();

/*
	WDV341 Intro PHP 
	Presenters CMS Example Code
	Logout Page
	
	This page is called when the user/customer clicks a link to their 'log out' page. 
	This page will destroy the session variable associated with this session, 
	close the data base connection and send the user to the login page or home page.
*/
if($_SESSION['validUser']=="yes")			//For a valid user that has set up a session
{
	$_SESSION['validUser'] = "";			//Set session variable to empty
	unset($_SESSION['validUser']);			//Unset the session variable
	session_destroy();						//destroy the session attached to this page
	header("Location:login.php");	//Redirect the user to the sign in page
	exit;									//If still here for some reason close the PHP session
}
else
{
	header("Location:login.php");	//Invalid user then send them to the login page
}

?>