<?php
	require_once('inc/user.class.php');
	$user = new User();
	
	if (isset($_REQUEST['user_id']) && $_REQUEST['user_id'] > 0)
	{	
    	$user->load($_REQUEST['user_id']);
	}
	
	if (isset($_POST['cancel'])) 
	{
   	 	header("location: user_list.php?cancel");
    	exit;
	}

	if (isset($_POST['user_id'])) 
	{
	
		if (isset($_FILES['user_image']) && $_FILES['user_image']['error'] == 0) 
    	{
        	$user->importUserImage($_FILES['user_image']);
    	}
    
    	$user->setData($_POST);
    
    	$errors = $user->validate();
    
    	if (count($errors) == 0) 
    	{
       	 	$user->save();
        
        	header("location: user_list.php?saved");
        	exit;
    	}
	}
	include_once("tpl/user_edit.tpl.php");

?>
    
