<?php

class User
{
	var $userId;
	var $username;
	var $user_password;
	var $userLevel;
	
	function getData()
    {
        $returnArray = array(
            'user_id' => $this->userId,
            'username' => $this->username,
            'password' => $this->user_password,
            'user_level' => $this->userLevel,
        );
        
        return $returnArray;
    }
	
	function setData($dataArray) 
    {
        $this->userId = $dataArray['user_id'];
        $this->username = $dataArray['username'];
        $this->user_password = $dataArray['password'];
        $this->userLevel = $dataArray['user_level'];
    }
    
	function checkLogin($user)
	{
			$success = false;
			$this->username = $user['loginUsername'];
        	$this->user_password = $user['loginPassword'];
        	$userId = -1;
       		 $db = mysqli_connect("localhost", "root", "");
        
        	if($db)
        	{
        		mysqli_select_db($db, 'wdv441');
        		$sql = "SELECT * FROM users WHERE username = '" . $this->username . "' AND password = '" . $this->user_password . "'";
        		//var_dump(user_id);
        		$rs = mysqli_query($db,$sql);
        		if (mysqli_num_rows($rs) == 1) 
            	{                
            		 $user = mysqli_fetch_assoc($rs);
                	//var_dump($userId);
                	//echo $userId['user_id'];
                	$this->setData($user);
                	//$_SESSION['userId'] = $user['user_id'];
                	//echo "User id is: ".$_SESSION['userId']."<br/>";
                	//echo "Login successful!";
                	$success = ($this->userId > 0 ? true : false);
            	}
            	else                 
            	{
                	echo mysqli_error($db);
                	die;                
            	}
        		
        	}
        	else
        	{
        		echo mysqli_error($db);
            	die;
        
        	}
			return $success;
	}
	
	function load($userId)
    {
        $success = false;
        
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $loadUserSQL = "SELECT * FROM users WHERE user_id = " . 
                mysqli_real_escape_string($db, $userId);            
            $rs = mysqli_query($db, $loadUserSQL);
            
            if ($rs) 
            {                
                $userData = mysqli_fetch_assoc($rs);
                $this->setData($userData);
                $success = ($this->userId > 0 ? true : false);
            }
            else                 
            {
                echo mysqli_error($db);
                die;                
            }
        } 
        else 
        {
            echo mysqli_error($db);
            die;
        }
        
        return $success;
    }
	
	function getUsers()
	{
		$rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getAuthorListSQL = "SELECT * FROM users";            
            $getAuthorListSQL .= " ORDER BY user_id";
                        
            $rs = mysqli_query($db, $getAuthorListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
	}
	
	function save()
    {
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
        
            if ($this->userId > 0) {
                // this is an update
                $userUpdateSQL = "UPDATE users SET " .
                    "username = '" . mysqli_real_escape_string($db, $this->username) . "', " .
                    "password = '" . mysqli_real_escape_string($db, $this->user_password) . "', " .
                    "user_level = '" . mysqli_real_escape_string($db, $this->userLevel). "' " .
                    "WHERE user_id = " . mysqli_real_escape_string($db, $this->userId);
					
                $rs = mysqli_query($db, $userUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                }
            }
            else 
            {
                // this is an insert
                $userInsertSQL = "INSERT INTO users SET " . 
                     "username = '" . mysqli_real_escape_string($db, $this->username) . "', " .
                    "password = '" . mysqli_real_escape_string($db, $this->user_password) . "', " .
                    "user_level = '" . mysqli_real_escape_string($db, $this->userLevel). "'";

                $rs = mysqli_query($db, $userInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                } 
                else 
                {
                    $this->userId = mysqli_insert_id($db);
                }

            }
        }
    }
    
    function validate() 
    {
        $errorsArray = array();
        
        if (empty($this->username)) 
        {
            $errorsArray['username'] = "Username is required.";
        }

        if (empty($this->user_password)) 
        {
            $errorsArray['password'] = "Password is required.";
        }

        if (empty($this->userLevel)) 
        {
            $errorsArray['userLevel'] = "UserLevel is required.";
        }
      
        return $errorsArray;
    }
    
    function importUserImage($fileUploadInfo) 
    {
        //var_dump($fileUploadInfo);die;
        
        //var_dump(pathinfo($fileUploadInfo['name']));die;
        
        $pathinfo = pathinfo($fileUploadInfo['name']);
        
        $imageName = "user_image" . $this->userId . "." . $pathinfo['extension'];
        $imagesFolder = dirname(__FILE__) . "/../images/";
        
        move_uploaded_file($fileUploadInfo['tmp_name'], $imagesFolder . $imageName);        
    }
    
    function getUserReportData($page, &$howMany, $level = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getUserListSQL = "SELECT * FROM users";

            $whereClause = "";
            
            if (!is_null($level) && !empty($level))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " user_level = '" . mysqli_real_escape_string($db, $level) . "'";
            }

            $getUserListSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );

            $getUserListSQL .= " LIMIT " . (($page - 1) * 5) . ", 5";
            
            $howMany = 0;
            $countSQL = "SELECT COUNT(*) AS result_count FROM users " . $whereClause;
            $rs = mysqli_query($db, $countSQL);     
            if ($rs) 
            {
                $row = mysqli_fetch_assoc($rs);
                $howMany = $row['result_count'];
            }
            
            //var_dump($howMany, $getUserListSQL);
            
            $rs = mysqli_query($db, $getUserListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }    
        
    function getUserReportDataForDownload($level = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getUserListSQL = "SELECT * FROM users";

            $whereClause = "";
            
            if (!is_null($level) && !empty($level))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " user_level = '" . mysqli_real_escape_string($db, $level) . "'";
            }

            
            $getUserListSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );
                        
            $rs = mysqli_query($db, $getUserListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }  
        }
        
        return $rs;
    }  
    
    function getWeatherWidget() 
    {
        $curl = curl_init();

        $url = 'http://localhost/wdv/aphp/week13homework/weather.widget.php';

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $widget_html = curl_exec($curl);

        curl_close($curl);
        
        return $widget_html;
    }      
    
}
?>