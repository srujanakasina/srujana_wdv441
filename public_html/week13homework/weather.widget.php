<?php

$curl = curl_init();

$url = 'http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20weather.forecast%20where%20woeid%20in%20(select%20woeid%20from%20geo.places(1)%20where%20text%3D%22Des%20Moines%2C%20ia%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys';

curl_setopt($curl, CURLOPT_URL, $url);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

$page_html = curl_exec($curl);

curl_close($curl);

$weather_array = json_decode($page_html, true);

//var_dump($weather_array);

include_once('tpl/weather.widget.tpl.php');
?>