<?php
session_start();

require_once("inc/user.class.php");

if($_SESSION['level'] == 'admin'||$_SESSION['level'] == 'moderator')
{
$user = new User();

$userReportData = null;

$howMany = 0;
if (isset($_GET['run_report'])) {
    $userReportData = $user->getUserReportData($_SESSION['reportPage'], $howMany, $_GET['user_level']);    
}

$onLastPage = ((($_SESSION['reportPage']) * 5) <= $howMany);

//var_dump($howMany);

$myHowManyVariable = 'howMany';

//var_dump($myHowManyVariable, $$myHowManyVariable);

$user = new User();

if (isset($_GET['download'])) {
    $downloadData = $user->getUserReportDataForDownload($_GET['user_level']);

    $fileName = "myUserReport_" . date("Y_m_d_His") . ".csv";
    
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $fileName . '"');
    
    while ($row = mysqli_fetch_assoc($downloadData)) 
    {
        $dataRowLine = implode("|", $row) . "\r\n";
        echo $dataRowLine;        
    }
    exit;
}

if (isset($_GET['next'])) 
{
    $_SESSION['reportPage']++;
} 
elseif (isset($_GET['prev'])) 
{
    $_SESSION['reportPage']--;
    if ($_SESSION['reportPage'] < 1) 
    {
        $_SESSION['reportPage'] = 1;
    }
} 
else 
{
    $_SESSION['reportPage'] = 1;
}

$_SERVER['QUERY_STRING'] = str_replace(array("&prev", "&next"), "", $_SERVER['QUERY_STRING']);

$weatherWidgetHTML = $user->getWeatherWidget();

if (is_null($userReportData)) {
    include_once("tpl/user_report.tpl.php");
} else {
    include_once("tpl/user_report_display.tpl.php");    
}
}
else{
	echo "Cannot view the page";
}

?>

