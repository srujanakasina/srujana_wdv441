<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Contact Form</title>
<style>
label {
	padding-left: 10px;
	display: inline-block;
	width: 150px;
	margin-top: 10px;
	font-size: medium;
}
input[type="text"] {
	width: 200px;
	height: 20px;
	border-radius: 5px;
	border: 1px solid #4996CC;
}
input[type="email"] {
	width: 200px;
	height: 20px;
	border-radius: 5px;
	border: 1px solid #4996CC;
}
textarea {
	border-radius: 5px;
	border: 1px solid #4996CC;
	width: 200px;
	margin-left:164px;
}
input[type="submit"],input[type="reset"] {
	width: 150px;
	height: 30px;
	border-radius: 5px;
	margin-top:10px;
}

.error	{
	color:red;	
}
</style>
</head>
<body>
<h2>Contact Form</h2>
<?php
if(!isset($_POST['button']))
{
	global $firstNameErrMsg, $lastNameErrMsg, $dobErrMsg, $emailErrMsg, $contentErrMsg, $firstName,$lastName,$inEmail,$dob,$content;
}
function validateFirstName()
{
	global $firstName, $validForm, $firstNameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
	$firstNameErrMsg = "";								//Clear the error message. 
	if($firstName=="")
	{
		$validForm = false;					//Invalid name so the form is invalid
		$firstNameErrMsg = "First Name is required";	//Error message for this validation
		return false;	
	}
	return true;
}

function validateLastName()
{
	global $lastName, $validForm, $lastNameErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
	$lastNameErrMsg = "";								//Clear the error message. 
	if($lastName=="")
	{
		$validForm = false;					//Invalid name so the form is invalid
		$lastNameErrMsg = "Last Name is required";	//Error message for this validation
		return false;	
	}
	return true;
}
function validateDob()
{

	global $dob, $validForm, $dobErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
	$dobErrMsg = "";								//Clear the error message. 
	if($dob=="")
	{
		$validForm = false;					//Invalid name so the form is invalid
		$dobErrMsg = "Date is required";	//Error message for this validation
		return false;	
	}
	else
	{
		if(!preg_match("/^(0[1-9]|1[0-2])\/(0[1-9]|1\d|2\d|3[01])\/(19|20)\d{2}$/", $dob))
		{
			$validForm = false;					//Invalid name so the form is invalid
			$dobErrMsg = "Invalid date";	//Error message for this validation
			return false;
		}
	}
	return true;
}

function validateEmail()
{
	global $inEmail, $validForm, $emailErrMsg;
	$emailErrMsg="";
	if($inEmail=="")
	{
		$validForm = false;
		$emailErrMsg="Field required";
		return false;
	}
	else
	{
		if(!preg_match("/[-0-9a-zA-Z.+_]+@[-0-9a-zA-Z.+_]+\.[a-zA-Z]{2,4}/", $inEmail)) 
		{
			$validForm = false;
  			$emailErrMsg = "Invalid email format"; 
  			return false;
  		}
	}
	return true;
}
function validateContent()
{
	global $content, $validForm, $contentErrMsg;		//Use the GLOBAL Version of these variables instead of making them local
	$contentErrMsg = "";								//Clear the error message. 
	if($content=="")
	{
		$validForm = false;					//Invalid name so the form is invalid
		$contentErrMsg = "Content required";	//Error message for this validation	
		return false;
	}
	return true;
}
function validateForm()
{
	$isFirstNameValid = validateFirstName();
	$isLastNameValid=validateLastName();
	$isDobValid=validateDob();
	$isEmailValid=validateEmail();
	$isContentValid=validateContent();
	
	if($isFirstNameValid && $isLastNameValid && $isDobValid && $isEmailValid && $isContentValid){
			return true;
		}
	return false;

}
if(isset($_POST['button']) )				//if the form has been submitted Validate the form data
{
	//pull data from the POST variables in order to validate their values
	$firstName = $_POST['fname'];
	$lastName = $_POST['lname'];
	$dob = $_POST['dob'];	
	$inEmail = $_POST['email'];	
	$content=$_POST['message'];
	

	
	$validForm = validateForm();					//Set form flag/switch to true.  Assumes a valid form so far
		
	if($validForm)	//Check the form flag.  If it is still true all the data is valid and the form is ready to process
	{
		// The form  data is valid and can be processed into your database.
		$_SESSION['fname']=$firstName;
		$_SESSION['lname']=$lastName;
		$_SESSION['dob']=$dob;
		$_SESSION['email']=$inEmail;
		$_SESSION['message']=$content;

		header("Location:contact_formHandler.php");
		exit();		//Finishes the page so it does not display the form again.
	}
	else			//The form has at least one invalid field.  It may have more.  All will be displayed.
	{
		//Load the original formdata back into the fields
		//Load the error messages onto the form.  Only invalid fields will have an error message.  Others will be blank.
		//Display the form back to the user for corrections.   The page will continue process from this point, displaying the updated form.
	}
}

?>


<form id="contact" method="post" name="contact" action="contactform.php">


<!-- text boxes are below -->
<label for "name">First Name</label>
<input type="text" name="fname" id="fname" value="<?php echo $firstName;  ?>"/><span class="error"><?php echo "$firstNameErrMsg"; ?></span>
<br/>
<label for "name">Last Name</label>
<input type="text" name="lname" id="lname" value="<?php echo $lastName;  ?>"/><span class="error"><?php echo "$lastNameErrMsg"; ?></span>
<br/>
<label for "name">Date of Birth</label>
<input type="text" name="dob" id="dob" value="<?php echo $dob;  ?>"/><span class="error"><?php echo "$dobErrMsg"; ?></span>
<br/>

<label for "email">Email</label>
<input type="email" name="email" id="email" size="40" value="<?php echo $inEmail;  ?>"/><span class="error"><?php echo "$emailErrMsg"; ?></span>
<br/>

<label for "message">Message</label><br/>
<textarea name="message" id="message" rows="5" size="40" ><?php echo $content;  ?></textarea><span class="error"><?php echo "$contentErrMsg"; ?></span>
<!-- input buttons are below -->
<div>
<input type="submit" name="button" value="Submit" class="button"/>
<input type="reset" name="button2" value="Cancel" class="button"/></div>
</form>
</body>
</html>
