<?php session_start(); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Contact Form</title>

</head>

<body>

<?php
date_default_timezone_set("America/Chicago");

$date = date('l F d, Y');
$time=date('h:i A');
//Emails the confirmation
include_once "lib/swift_required.php";
	$body= "<h2 style='color:gray;'>Form Data</h2>";			//"$body" stores the content of the email
	$body.="<table style='background-color:#94C5D1;'>";
	$body.="<tr><td>First Name:".$_SESSION['fname']."</td></tr>";
	$body.="<tr><td>Last Name:".$_SESSION['lname']."</td></tr>";
	$body.="<tr><td>Last Name:".$_SESSION['dob']."</td></tr>";
	$body.="<tr><td>Last Name:".$_SESSION['email']."</td></tr>";
	$body.="<tr><td>Message:".$_SESSION['message']."</td></tr>";
	$body.="</table>";
	$body.="<p style='font-size:1.5em;color:green;'>The form has been submitted on ". $date ." at ". $time."</p>";
	echo $body;
 // This is your From email address
 $from = array('skasina@dmacc.edu' => 'Srujana');
 // Email recipients
 $to = array($_SESSION['email']=>$_SESSION['name'],'srujanakasina@gmail.com','gbgrandberg@dmacc.edu');
 // Email subject
 $subject = " Contact Email";	
 // Login credentials
 $username = '';
 $password = '';

 // Setup Swift mailer parameters
 $transport = Swift_SmtpTransport::newInstance('smtp.sendgrid.net', 587);
 $transport->setUsername($username);
 $transport->setPassword($password);
 $swift = Swift_Mailer::newInstance($transport);

 // Create a message (subject)
 $message = new Swift_Message($subject);

 // attach the body of the email
 $message->setFrom($from);
 $message->setBody($body, 'text/html');
 $message->setTo($to);
 
 
if ($recipients = $swift->send($message, $failures))
 {
     echo "Message delivered successfully";
 }
 else
 {
    echo "Message deliver failed";
  }
?>
</body>
</html>
