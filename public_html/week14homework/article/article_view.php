<?php
require_once('inc/news_article.class.php');

$newsArticle = new NewsArticle();

if (!$newsArticle->load($_GET['article_id'])) {
    header("location: 404.php");
    exit;
}

//var_dump($newsArticle);

include_once('tpl/article_view.tpl.php');
?>