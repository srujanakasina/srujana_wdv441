<html>
    <body>
        <table border="1">
            <thead>
                <th><a href="article_list.php?sort_column=article_title">Article Title</a></th>
                <th><a href="article_list.php?sort_column=article_author">Article Author</a></th>
                <th><a href="article_list.php?sort_column=article_date">Article Date</a></th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>  

            <?php while ($row = mysqli_fetch_assoc($articleReportData)) 
            { ?>
                <tr>
                    <td>
                        <?php echo $row['article_title']; ?>
                    </td>
                    <td>
                        <?php echo $row['article_author']; ?>
                    </td>
                    <td>
                        <?php echo $row['article_date']; ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&prev">Previous</a>&nbsp;
        <?php if (!$onLastPage) { ?>
            <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&next">Next</a><br>
        <?php } ?>
            <a href="article_report.php">Run Another</a><br>        
            <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&download">Download</a>
    </body>
</html>