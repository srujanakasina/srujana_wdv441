<?php

class NewsArticle 
{
    var $articleId;
    var $articleTitle;
    var $articleContent;
    var $articleAuthor;
    var $articleDate;

    function getData()
    {
        $returnArray = array(
            'article_id' => $this->articleId,
            'article_title' => $this->articleTitle,
            'article_content' => $this->articleContent,
            'article_author' => $this->articleAuthor,
            'article_date' => $this->articleDate
        );
        
        return $returnArray;
    }
    
    function setData($dataArray) 
    {
        $this->articleId = $dataArray['article_id'];
        $this->articleTitle = $dataArray['article_title'];
        $this->articleContent = $dataArray['article_content'];
        $this->articleAuthor = $dataArray['article_author'];
        $this->articleDate = $dataArray['article_date'];
    }
    
    function load($articleId)
    {
        $success = false;
        
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $loadArticleSQL = "SELECT * FROM news_article WHERE article_id = " . 
                mysqli_real_escape_string($db, $articleId);            
            //var_dump($loadArticleSQL);die;
            $rs = mysqli_query($db, $loadArticleSQL);
            
            if ($rs) 
            {                
                $articleData = mysqli_fetch_assoc($rs);
                //var_dump($articleData);die;
                $this->setData($articleData);
                $success = ($this->articleId > 0 ? true : false);
            }
            else                 
            {
                echo mysqli_error($db);
                die;                
            }
        } 
        else 
        {
            echo mysqli_error($db);
            die;
        }
        
        return $success;
    }
    
    function save()
    {
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
        
            if ($this->articleId > 0) {
                // this is an update
                $articleUpdateSQL = "UPDATE news_article SET " .
                    "article_title = '" . mysqli_real_escape_string($db, $this->articleTitle) . "', " .
                    "article_content = '" . mysqli_real_escape_string($db, $this->articleContent) . "', " .
                    "article_author = '" . mysqli_real_escape_string($db, $this->articleAuthor) . "', " .
                    "article_date = '" . mysqli_real_escape_string($db, $this->articleDate) . "' " .
                    "WHERE article_id = " . mysqli_real_escape_string($db, $this->articleId);

                $rs = mysqli_query($db, $articleUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                }
            }
            else 
            {
                // this is an insert
                $articleInsertSQL = "INSERT INTO news_article SET " . 
                    "article_title = '" . mysqli_real_escape_string($db, $this->articleTitle) . "', " .
                    "article_content = '" . mysqli_real_escape_string($db, $this->articleContent) . "', " .
                    "article_author = '" . mysqli_real_escape_string($db, $this->articleAuthor) . "', " .
                    "article_date = '" . mysqli_real_escape_string($db, $this->articleDate) . "'";

                $rs = mysqli_query($db, $articleInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                } 
                else 
                {
                    $this->articleId = mysqli_insert_id($db);
                }

            }
        }
    }
    
    function validate() 
    {
        $errorsArray = array();
        
        if (empty($this->articleTitle)) 
        {
            $errorsArray['articleTitle'] = "Article Title is required.";
        }

        if (empty($this->articleContent)) 
        {
            $errorsArray['articleContent'] = "Article Content is required.";
        }

        if (empty($this->articleAuthor)) 
        {
            $errorsArray['articleAuthor'] = "Article Author is required.";
        }
      
        return $errorsArray;
    }
    
    function getArticleList($sort_column = null, $authorFilter = null, $searchText = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT * FROM news_article";

            if (!is_null($authorFilter))
            {
                $getArticleListSQL .= " WHERE article_author = '" . mysqli_real_escape_string($db, $authorFilter) . "'";
            }

            if (!is_null($searchText))
            {
                $getArticleListSQL .= " WHERE " . 
                    "article_title LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " .
                    "article_content LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " . 
                    "article_date LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%' OR " . 
                    "article_author LIKE '%" . mysqli_real_escape_string($db, $searchText) . "%'";
            }
            //var_dump($getArticleListSQL);die;
            if (!is_null($sort_column))
            {
                $getArticleListSQL .= " ORDER BY " . $sort_column;
            }
                        
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }
    
    function getArticleList2() 
    {
        $rs = null;
            
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT article_title, article_author, article_date FROM news_article";
            
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }

    function getAuthorList() 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getAuthorListSQL = "SELECT DISTINCT article_author FROM news_article";            
            $getAuthorListSQL .= " ORDER BY article_author";
                        
            $rs = mysqli_query($db, $getAuthorListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }
    
    function exportNewsArticles($filename) 
    {
        $exportPath = dirname(__FILE__) . "/../../data/";
                
        $exportFile = fopen($exportPath . "/" . $filename, "w");
        
        if ($exportFile) 
        {
        
            $articleList = $this->getArticleList();
            
            while ($row = mysqli_fetch_assoc($articleList)) 
            {
                //var_dump($row);
                fputcsv($exportFile, $row);
            }
            
            fclose($exportFile);
        }
        
        return file_exists($exportPath . "/" . $filename);
    }
    
    function importNewsArticles($filepath)
    {
        if (file_exists($filepath)) 
        {
            $importFile = fopen($filepath, "r");
            
            if ($importFile) 
            {
                while (!feof($importFile)) 
                {
                    $rowData = fgetcsv($importFile);
                    
                    $this->articleId = $rowData[0];
                    $this->articleTitle = $rowData[1];
                    $this->articleContent = $rowData[2];
                    $this->articleAuthor = $rowData[3];
                    $this->articleDate = $rowData[4];
                 
                    if (!empty($this->articleTitle))
                    {                    
                        $this->save();                    
                    }
                }

                fclose($importFile);                
            }            
        }
    }
    
    function importNewsImage($fileUploadInfo) 
    {
        //var_dump($fileUploadInfo);die;
        
        //var_dump(pathinfo($fileUploadInfo['name']));die;
        
        $pathinfo = pathinfo($fileUploadInfo['name']);
        
        $imageName = "news_article_" . $this->articleId . "." . $pathinfo['extension'];
        $imagesFolder = dirname(__FILE__) . "/../images/";
        
        move_uploaded_file($fileUploadInfo['tmp_name'], $imagesFolder . $imageName);        
    }
    
    function getImage() 
    {
        $image = null;
        
        $imagesFolder = dirname(__FILE__) . "/../images/";
        $imageName = "news_article_" . $this->articleId . ".png";
        
        if (file_exists($imagesFolder . $imageName)) 
        {
            $image = $imageName;
        }
        
        return $image;
    }
    
    function articleView() 
    {
        ob_start();
        include('tpl/article_view.tpl.php');
        ob_end_flush();
        $output = ob_get_contents();
        
        return $output;
    }

    function getArticleReportData($page, &$howMany, $authorFilter = null, $startDate = null, $endDate = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT * FROM news_article";

            $whereClause = "";
            
            if (!is_null($authorFilter) && !empty($authorFilter))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " article_author = '" . mysqli_real_escape_string($db, $authorFilter) . "'";
            }

            if (!is_null($startDate) && !empty($startDate) && !is_null($endDate) && !empty($endDate))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . 
                    " article_date BETWEEN " . 
                    "'" . mysqli_real_escape_string($db, $startDate) . "' AND " . 
                    "'" . mysqli_real_escape_string($db, $endDate) . "'";
            }
            
            $getArticleListSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );

            $getArticleListSQL .= " LIMIT " . (($page - 1) * 5) . ", 5";
            
            $howMany = 0;
            $countSQL = "SELECT COUNT(*) AS result_count FROM news_article " . $whereClause;
            $rs = mysqli_query($db, $countSQL);     
            if ($rs) 
            {
                $row = mysqli_fetch_assoc($rs);
                $howMany = $row['result_count'];
            }
            
            //var_dump($howMany, $getArticleListSQL);
            
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }    
        
    function getArticleReportDataForDownload($authorFilter = null, $startDate = null, $endDate = null) 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT * FROM news_article";

            $whereClause = "";
            
            if (!is_null($authorFilter) && !empty($authorFilter))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . " article_author = '" . mysqli_real_escape_string($db, $authorFilter) . "'";
            }

            if (!is_null($startDate) && !empty($startDate) && !is_null($endDate) && !empty($endDate))
            {
                $whereClause .= (!empty($whereClause) ? "AND" : "" ) . 
                    " article_date BETWEEN " . 
                    "'" . mysqli_real_escape_string($db, $startDate) . "' AND " . 
                    "'" . mysqli_real_escape_string($db, $endDate) . "'";
            }
            
            $getArticleListSQL .= (!empty($whereClause) ? " WHERE " . $whereClause : "" );
                        
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }
            
            
            
            
        }
        
        return $rs;
    }    
    
    function getNewsArticleWidget()
    {
        $curl = curl_init();

        $url = 'http://localhost/wdv/aphp/PHPfinal/article/article_list.widget.php';

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $widget_html = curl_exec($curl);

        curl_close($curl);
        
        return $widget_html;
    }    
}
?>