<?php
require_once('inc/news_article.class.php');

$newsArticle = new NewsArticle();

if (!$newsArticle->load($_GET['article_id'])) {
    header("location: 404.php");
    exit;
}

echo json_encode($newsArticle->getData());
?>