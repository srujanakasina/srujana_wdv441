<?php
session_start();

require_once("inc/news_article.class.php");

$news = new NewsArticle();

if (isset($_GET['download'])) {
    $downloadData = $news->getArticleReportDataForDownload($_GET['author_filter'], $_GET['article_start_date'], $_GET['article_end_date']);

    $fileName = "myCoolReport_" . date("Y_m_d_His") . ".csv";
    
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $fileName . '"');
    
    while ($row = mysqli_fetch_assoc($downloadData)) 
    {
        $dataRowLine = implode("|", $row) . "\r\n";
        echo $dataRowLine;
    }
    exit;
}

if (isset($_GET['next'])) 
{
    $_SESSION['reportPage']++;
} 
elseif (isset($_GET['prev'])) 
{
    $_SESSION['reportPage']--;
    if ($_SESSION['reportPage'] < 1) 
    {
        $_SESSION['reportPage'] = 1;
    }
} 
else 
{
    $_SESSION['reportPage'] = 1;
}

$_SERVER['QUERY_STRING'] = str_replace(array("&prev", "&next"), "", $_SERVER['QUERY_STRING']);

//var_dump($_SERVER);

$news = new NewsArticle();

$articleReportData = null;

$howMany = 0;
if (isset($_GET['run_report'])) {
    $articleReportData = $news->getArticleReportData($_SESSION['reportPage'], $howMany, $_GET['author_filter'], $_GET['article_start_date'], $_GET['article_end_date']);    
}

$onLastPage = ((($_SESSION['reportPage']) * 5) <= $howMany);

//var_dump($howMany);

$myHowManyVariable = 'howMany';

//var_dump($myHowManyVariable, $$myHowManyVariable);

$newsArticleWidgetHTML = $news->getNewsArticleWidget();

if (is_null($articleReportData)) {
    include_once("tpl/article_report.tpl.php");
} else {
    include_once("tpl/article_report_display.tpl.php");    
}
?>
