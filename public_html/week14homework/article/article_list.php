<?php
require_once('inc/news_article.class.php');

$news = new NewsArticle();

$newsList = $news->getArticleList(
    (isset($_GET['sort_column']) ? $_GET['sort_column'] : null),
    (isset($_GET['author_filter']) && !empty($_GET['author_filter']) ? $_GET['author_filter'] : null),
    (isset($_GET['search_text']) && !empty($_GET['search_text']) ? $_GET['search_text'] : null)
);
$newsList2 = $news->getArticleList2();

$authorList = $news->getAuthorList();

//var_dump($authorList);

//var_dump($newsList);


if (isset($_GET['cancel']))
{
    $message = "Edit cancelled";
} elseif (isset($_GET['saved']))
{
    $message = "Article Saved";
} else 
{
    $message = null;
}

include_once("tpl/article_list.tpl.php");
?>
