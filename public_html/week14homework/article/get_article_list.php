<?php
include_once('inc/news_article.class.php');

$newsArticle = new NewsArticle();

$articleList = $newsArticle->getArticleList();

$jsonArray = array();
$jsonArray['news_article'] = array();

while ($row = mysqli_fetch_assoc($articleList)) 
{
        $jsonArray['news_article'][] = $row;
}

$jsonArray['authors'] = array();

$authorList = $newsArticle->getAuthorList();

while ($row = mysqli_fetch_assoc($authorList)) 
{
        $jsonArray['authors'][] = $row;
}

echo json_encode($jsonArray);
?>