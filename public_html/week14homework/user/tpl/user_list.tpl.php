<html>
    <body>
        
       <?php if ($message) 
        { ?>
            <div><?php echo $message; ?></div>
        <?php } ?><br>
        
        <a href="user_edit.php">Create New User</a>
        <br>
        <table border="1">
            <thead>
                <th><a href="user_list.php?sort_column=username">Username</a></th>
                <th><a href="user_list.php?sort_column=password">Password</a></th>
                <th><a href="user_list.php?sort_column=user_level">UserLevel</a></th>
                <th>&nbsp;</th>
            </thead>  

            <?php while ($row = mysqli_fetch_assoc($usersList)) 
            { ?>
                <tr>
                    <td>
                        <?php echo $row['username']; ?>
                    </td>
                    <td>
                        <?php echo $row['password']; ?>
                    </td>
                    <td>
                        <?php echo $row['user_level']; ?>
                    </td>
                    <td>
                        <a href="user_edit.php?user_id=<?php echo $row['user_id']; ?>">Edit</a>
                    </td>
                    </tr>
            <?php } ?>

<!--                
            <?php while ($row = mysqli_fetch_assoc($newsList2)) 
            { ?>
                <tr>
                    <?php foreach ($row as $column_name => $data)
                    { ?>
                        <td>
                            <?php echo $data; ?>
                        </td>                        
                    <?php } ?>                    
                </tr>
            <?php } ?>
-->

        </table>
    <br>
        <a href="user_report.php">User Report</a>
        <br>
       <a href="logout.php">Logout</a>
    </body>
</html>