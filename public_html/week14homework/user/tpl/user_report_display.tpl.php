<html>
    <body>
        <table border="1">
            <thead>
                <th><a href="user_list.php?sort_column=username">UserName</a></th>
                <th><a href="user_list.php?sort_column=password">Password</a></th>
                <th><a href="user_list.php?sort_column=user_level">UserLevel</a></th>
            </thead>  

            <?php while ($row = mysqli_fetch_assoc($userReportData)) 
            { ?>
                <tr>
                    <td>
                        <?php echo $row['username']; ?>
                    </td>
                    <td>
                        <?php echo $row['password']; ?>
                    </td>
                    <td>
                        <?php echo $row['user_level']; ?>
                    </td>
                </tr>
            <?php } ?>
        </table>
        <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&prev">Previous</a>&nbsp;
        <?php if (!$onLastPage) { ?>
            <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&next">Next</a><br>
        <?php } ?>
            <a href="user_report.php">Run Another</a><br>        
            <a href="<?= $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING']; ?>&download">Download</a>
        <br>
        <a href="logout.php">Logout</a>
    </body>
</html>