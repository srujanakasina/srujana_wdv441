<?php
require_once('inc/user.class.php');

$user = new User();

if (!$user->load($_GET['user_id'])) {
    header("location: 404.php");
    exit;
}

echo json_encode($user->getData());
?>