<?php
	require_once('inc/user.class.php');

	$user = new User();

	$usersList = $user->getUsers();

	if (isset($_GET['cancel']))
	{
	    $message = "Edit cancelled";
	} elseif (isset($_GET['saved']))
	{
		$message = "User Saved";
	} else 
	{
   	 	$message = null;
	}

	include_once("tpl/user_list.tpl.php");
?>
