<html>
    <body>
        
        <?php if (isset($errors) && count($errors) > 0) { ?>
            <!-- html -->
            <div>
                <ul>
                    <?php foreach ($errors as $field => $errorMessage) 
                    {?>
                        <li><?php echo $errorMessage; ?></li>
                    <?php } ?>                    
                </ul>
            </div>
        <?php } ?>

        <?php //var_dump($errors); ?>
               
            <form action="<?php echo $_SERVER['SCRIPT_NAME']; ?>" method="POST">
                page title: <input type="text" name="page_title" value="<?php echo $cmsPage->pageTitle; ?>"/><br>
                <span><?php if (isset($errors['pageTitle'])) { ?><?php echo $errors['articleContent']; ?><?php } ?></span>
                content: <br>
                <textarea name="content">
                    <?php echo $cmsPage->content; ?>
                </textarea><br>
                <span><?php if (isset($errors['articleAuthor'])) { ?><?php echo $errors['articleAuthor']; ?><?php } ?></span>
                meta tags: <input type="text" name="meta_tags" value="<?php echo $cmsPage->metaTags; ?>"/><br>
                <span><?php if (isset($errors['articleDate'])) { ?><?php echo $errors['articleDate']; ?><?php } ?></span>
                h1: <input type="text" name="h1" value="<?php echo $cmsPage->h1; ?>"/><br>            
                url key: <input type="text" name="url_key" value="<?php echo $cmsPage->urlKey; ?>"/><br>            
                <input type="hidden" name="cms_page_id" value="<?php echo $cmsPage->cmsPageId; ?>"/><br>
                <input type="submit" name="submit">
                <input type="submit" name="cancel" value="cancel">            
            </form>        
    </body>
</html>