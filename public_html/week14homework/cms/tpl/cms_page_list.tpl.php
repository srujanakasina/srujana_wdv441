<html>
    <body>
        
       <?php if ($message) 
        { ?>
            <div><?php echo $message; ?></div>
        <?php } ?><br>
        
        <a href="cms_page_edit.php">Create New Content</a>
       
        <br>
        <table border="1">
            <thead>
                <th>Title</th>
                <th>Metatags</th>
                <th>Heading</th>
                <th>Content</th>
                <th>Url Key</th>
                <th>&nbsp;</th>
                <th>&nbsp;</th>
            </thead>  

            <?php while ($row = mysqli_fetch_assoc($cmsList)) 
            { ?>
                <tr>
                    <td>
                        <?php echo $row['page_title']; ?>
                    </td>
                    <td>
                        <?php echo $row['meta_tags']; ?>
                    </td>
                    <td>
                        <?php echo $row['h1']; ?>
                    </td>
                    <td>
                        <?php echo $row['content']; ?>
                    </td>
                    <td>
                        <?php echo $row['url_key']; ?>
                    </td>
                    <td>
                        <a href="cms_page_edit.php?cms_page_id=<?php echo $row['cms_page_id']; ?>">Edit</a>
                    </td>
                    <td>
                        <a href="cms_page_view.php?cms_page_id=<?php echo $row['cms_page_id']; ?>">View</a>
                    </td>
                </tr>
            <?php } ?>

<!--                
            <?php while ($row = mysqli_fetch_assoc($newsList2)) 
            { ?>
                <tr>
                    <?php foreach ($row as $column_name => $data)
                    { ?>
                        <td>
                            <?php echo $data; ?>
                        </td>                        
                    <?php } ?>                    
                </tr>
            <?php } ?>
-->

        </table>
    </body>
</html>