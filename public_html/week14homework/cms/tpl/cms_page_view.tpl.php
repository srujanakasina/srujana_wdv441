<html>
    <body>
        <table>
            <tr>
                <td>
                    Title:
                </td>
                <td>
                    <?php echo $cmsPage->pageTitle; ?>
                </td>                
            </tr>
            <tr>
                <td>
                    MetaTags:
                </td>
                <td>
                    <?php echo $cmsPage->metaTags; ?>
                </td>                
            </tr>
            <tr>
                <td>
                    Heading:
                </td>
                <td>
                    <?php echo $cmsPage->h1; ?>
                </td>                
            </tr>
            
            <tr>
            	<td>
                    Content:
                </td>
                <td colspan="2">
                    <?php echo $cmsPage->content; ?>
                </td>                
            </tr>
            
             <tr>
                <td>
                    Url Key:
                </td>
                <td>
                    <?php echo $cmsPage->urlKey; ?>
                </td>                
            </tr>
            
        </table>
        <a href="cms_page_list.php">Back to List</a>
    </body>
</html>