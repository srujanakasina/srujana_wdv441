<html>
    <head>
        <title>
            <?php echo $cms->pageTitle; ?> 
        </title>
        
        <meta name="keywords" content="<?php echo $cms->metaTags; ?>"> 
    </head>
    
    <body>
        <h1><?php echo $cms->h1; ?></h1>
        <?php echo nl2br($cms->content); ?>
        <br>
        <br>
        <br>
        <a href="cms_page_list.php">CMS Page List</a>
        <br>
        <a href="../article/article_list.php">Article List</a>
        <br>
        <a href="../user/user_list.php">User List</a>
    </body>
</html>