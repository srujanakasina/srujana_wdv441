<?php
require_once('inc/cms_page.class.php');

$cms = new CMSPage();

if (isset($_GET['key'])) 
{
    if (!$cms->loadByURLKey($_GET['key'])) 
    {
        header("location: 404.php");
        exit;        
    }
} 
else 
{
    header("location: 404.php");
    exit;
}

include_once("tpl/index.tpl.php");
?>