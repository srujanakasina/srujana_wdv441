<?php
require_once('inc/cms_page.class.php');

$cmsPage = new CMSPage();

if (isset($_REQUEST['cms_page_id']) && $_REQUEST['cms_page_id'] > 0)
{
    $cmsPage->load($_REQUEST['cms_page_id']);
}

if (isset($_POST['cancel'])) 
{
    header("location: cms_page_list.php?cancel");
    exit;
}

if (isset($_POST['cms_page_id'])) 
{
    
    $cmsPage->setData($_POST);
    
    $errors = $cmsPage->validate();
    
    if (count($errors) == 0) 
    {
        $cmsPage->save();
        
        header("location: cms_page_list.php?saved");
        exit;
    }
    
}

include("tpl/cms_page_edit.tpl.php");
?>