<?php
require_once('inc/cms_page.class.php');

$cmsPage = new CMSPage();

if (!$cmsPage->load($_GET['cms_page_id'])) {
    header("location: 404.php");
    exit;
}

//var_dump($newsArticle);

include_once('tpl/cms_page_view.tpl.php');
?>