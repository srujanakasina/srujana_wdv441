<?php

class CMSPage 
{
    var $cmsPageId;
    var $pageTitle;
    var $metaTags;
    var $h1;
    var $content;
    var $urlKey;

    function getData()
    {
        $returnArray = array(
            'cms_page_id' => $this->cmsPageId,
            'page_title' => $this->pageTitle,
            'meta_tags' => $this->metaTags,
            'h1' => $this->h1,
            'content' => $this->content,
            'url_key' => $this->urlKey            
        );
        
        return $returnArray;
    }
    
    function setData($dataArray) 
    {
        $this->cmsPageId = $dataArray['cms_page_id'];
        $this->pageTitle = $dataArray['page_title'];
        $this->metaTags = $dataArray['meta_tags'];
        $this->h1 = $dataArray['h1'];
        $this->content = $dataArray['content'];
        $this->urlKey = $dataArray['url_key'];
    }
    
    function loadByURLKey($urlKey) 
    {
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $loadArticleSQL = "SELECT cms_page_id FROM cms_page WHERE url_key = '" . 
                mysqli_real_escape_string($db, $urlKey) . "'";            
            //var_dump($loadArticleSQL);die;
            $rs = mysqli_query($db, $loadArticleSQL);
            
            if ($rs) 
            {                
                $cmsPageData = mysqli_fetch_assoc($rs);
                //var_dump($cmsPageData);die;
                if (isset($cmsPageData['cms_page_id'])) 
                {
                    return $this->load($cmsPageData['cms_page_id']);
                }
            }
        }
        
        return false;        
    }
    
    function load($cmsPageId)
    {
        $success = false;
        
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $loadArticleSQL = "SELECT * FROM cms_page WHERE cms_page_id = " . 
                mysqli_real_escape_string($db, $cmsPageId);            
            //var_dump($loadArticleSQL);die;
            $rs = mysqli_query($db, $loadArticleSQL);
            
            if ($rs) 
            {                
                $cmsPageData = mysqli_fetch_assoc($rs);
//                var_dump($cmsPageData);die;
                $this->setData($cmsPageData);
                $success = ($this->cmsPageId > 0 ? true : false);
            }
            else                 
            {
                echo mysqli_error($db);
                die;                
            }
        } 
        else 
        {
            echo mysqli_error($db);
            die;
        }
        
        return $success;
    }
    
    function save()
    {
        $db = mysqli_connect("localhost", "root", "");
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
        
            if ($this->cmsPageId > 0) {
                // this is an update
                $articleUpdateSQL = "UPDATE cms_page SET " .
                    "page_title = '" . mysqli_real_escape_string($db, $this->pageTitle) . "', " .
                    "meta_tags = '" . mysqli_real_escape_string($db, $this->metaTags) . "', " .
                    "h1 = '" . mysqli_real_escape_string($db, $this->h1) . "', " .
                    "content = '" . mysqli_real_escape_string($db, $this->content) . "', " .
                    "url_key = '" . mysqli_real_escape_string($db, $this->urlKey) . "' " .
                    "WHERE cms_page_id = " . mysqli_real_escape_string($db, $this->cmsPageId);
                
                $rs = mysqli_query($db, $articleUpdateSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                }
            }
            else 
            {
                // this is an insert
                $articleInsertSQL = "INSERT INTO cms_page SET " . 
                    "page_title = '" . mysqli_real_escape_string($db, $this->pageTitle) . "', " .
                    "meta_tags = '" . mysqli_real_escape_string($db, $this->metaTags) . "', " .
                    "h1 = '" . mysqli_real_escape_string($db, $this->h1) . "', " .
                    "url_key = '" . mysqli_real_escape_string($db, $this->urlKey) . "', " .
                    "content = '" . mysqli_real_escape_string($db, $this->content) . "'";

                $rs = mysqli_query($db, $articleInsertSQL);     
                if (!$rs) {
                    echo mysqli_error($db);
                    die;
                } 
                else 
                {
                    $this->cmsPageId = mysqli_insert_id($db);
                }

            }
        }
    }
    
    function validate() 
    {
        $errorsArray = array();
              
        return $errorsArray;
    }
    
    function getCMSPageList() 
    {
        $rs = null;
        
        $db = mysqli_connect("localhost", "root", "");
        
        if ($db) 
        {
            mysqli_select_db($db, 'wdv441');
            
            $getArticleListSQL = "SELECT * FROM cms_page";
                        
            $rs = mysqli_query($db, $getArticleListSQL);     
            if (!$rs) 
            {
                echo mysqli_error($db);
                die;
            }            
        }
        
        return $rs;
    }
    
    function cmsView() 
    {
        ob_start();
        include('tpl/cms_page_view.tpl.php');
        ob_end_flush();
        $output = ob_get_contents();
        
        return $output;
    }
    
}
?>