<?php
require_once('inc/cms_page.class.php');

$cmsPage = new CMSPage();

$cmsList = $cmsPage->getCMSPageList();


if (isset($_GET['cancel']))
{
    $message = "Add/Edit cancelled";
} elseif (isset($_GET['saved']))
{
    $message = "Article Saved";
} else 
{
    $message = null;
}

include_once("tpl/cms_page_list.tpl.php");
?>